#!/bin/zsh
# from https://blog.shikisoft.com/ruby-aws-lambda-sam-cli-rds-mysql/
LAMBDA_SG="sg-0c5864bc42308abaa"
LAMBDA_SUBNETS="subnet-e6733080,subnet-3f8ac660,subnet-1b0b9c2a,subnet-443b3e4a,subnet-a6094087,subnet-82c8dbcf"

set -x
sam deploy --parameter-overrides LambdaSg=$LAMBDA_SG LambdaSubnets=$LAMBDA_SUBNETS
