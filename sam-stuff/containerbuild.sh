#/bin/zsh
set -x
poetry export -f requirements.txt --without-hashes > src/requirements.txt
sam build --use-container
rm src/requirements.txt

