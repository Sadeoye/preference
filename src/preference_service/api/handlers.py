from aws_lambda_powertools import Logger
from aws_lambda_powertools.logging import correlation_paths

from aws_lambda_powertools.utilities.typing import LambdaContext
from preference_service.common.apiresponses import BARDParameterValidationException, handle_api_response
from preference_service.common import db
from preference_service.common import validation_utils

# tracer = Tracer()
logger = Logger()
db = db.PreferencesDB()


# TODO: Change the below to API_GATEWAY_HTTP when SAM deployment is changed
# @tracer.capture_lambda_handler
@logger.inject_lambda_context(log_event=True,
                              clear_state=True,
                              correlation_id_path=correlation_paths.API_GATEWAY_REST)
@handle_api_response
def get_preference(event, context):

    assert event.get("httpMethod") == "GET"

    params = event["pathParameters"]
    user_id = params["userId"]
    client_code = params["clientCode"]
    category = params["category"]
    item_name = params["itemName"]

    logger.append_keys(user_id=user_id)
    logger.info("Getting preference")

    if not validation_utils.is_positive_int(user_id):
        raise BARDParameterValidationException(
            element="user_id", invalid_value=user_id, message="Invalid User ID"
        )

    logger.debug(
        {
            "user": user_id,
            "clientCode": client_code,
            "category": category,
            "itemName": item_name,
        }
    )

    preference = db.get_preference(
        user_id=user_id,
        client_code=client_code,
        category=category,
        item_name=item_name,
    )

    return preference
