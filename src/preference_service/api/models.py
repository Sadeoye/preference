# generated by datamodel-codegen:
#   filename:  preference_service_oas.json
#   timestamp: 2022-02-15T00:13:45+00:00

from __future__ import annotations

from typing import Any, Dict, List, Optional

from pydantic import BaseModel, Field, conint, constr


class Preference(BaseModel):

    class Config:
        allow_population_by_field_name = True

    type: str = Field(
        ..., description='The type of this resource', example='preference'
    )
    userId: conint(ge=0) = Field(..., description='User ID', example=1234567, alias='user_id')
    clientCode: constr(min_length=2, max_length=2) = Field(
        ...,
        description='Client Code (BI=BARD iOS, BA=BARD Android, BW=BARD Web, etc.)',
        example='BI',
        alias='client_code',
    )
    category: str = Field(
        ..., description='Preference category', example='Screen Settings'
    )
    itemName: str = Field(
        ..., description='The name of this preference item', example='Background Color', alias='item_name'
    )
    itemValue: Optional[Dict[str, Any]] = Field(
        None,
        description='The value for this preference item.\nA preference item can be any valid JSON object or it can be an individual value\nthat could be stored as the "value" portion of a JSON element (e.g.: 42, "blue").\n',
        example='Purple',
        alias='item_value'
    )


class PreferenceList(BaseModel):
    preferences: List[Preference]


class PreferenceItem(BaseModel):
    itemName: Optional[str] = None
    itemValue: Optional[str] = None


class PreferenceItemList(BaseModel):
    preferenceItems: List[PreferenceItem]


class PreferenceResponse(BaseModel):
    requestId: str = Field(..., example='ABCD1234')
    data: Optional[Preference] = None


class PreferenceListResponse(BaseModel):
    requestId: str = Field(..., example='ABCD1234')
    data: Optional[List[Preference]] = None


class Source(BaseModel):
    pointer: Optional[str] = Field(
        None,
        description='A JSON Pointer to the associated entity in the request document',
    )
    parameter: Optional[str] = Field(
        None,
        description='A string indicating which URI query or path parameter caused the error',
    )


class Error(BaseModel):
    code: Optional[str] = Field(
        None, description='Application-specific error code', example='PREFERENCES-0101'
    )
    title: str = Field(
        ...,
        description='A short summary of the problem that does not change on each occurrence of the problem',
        example='Invalid parameter',
    )
    detail: Optional[str] = Field(
        None,
        description='An explanation specific to this occurrence of the problem',
        example="Table cannot be sorted by the column titled '42'",
    )
    source: Optional[Source] = Field(
        None, description='An object containing references to the source of the error'
    )


class BARD2ErrorSchema(BaseModel):
    requestId: str = Field(..., example='ABCD1234')
    errors: List[Error]


class AnyValue(BaseModel):
    __root__: Any = Field(
        ..., description='Can be any value - string, number, boolean, array or object.'
    )


class ApiErrorResponse(BaseModel):
    __root__: BARD2ErrorSchema
