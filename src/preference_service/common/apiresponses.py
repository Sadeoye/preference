from functools import wraps

from pydantic import BaseModel

from preference_service.common import apiutils
from preference_service.api import models

ERROR_PREFERENCE_UNKNOWN_ERROR = "PREFERENCE-0100"
ERROR_PREFERENCE_MISSING_PARAMETER = "PREFERENCE-0101"
ERROR_PREFERENCE_INVALID_PARAMETER_VALUE = "PREFERENCE-0102"
ERROR_PREFERENCE_INVALID_REQUEST = "PREFERENCE-0103"


class BARDParameterValidationException(Exception):
    """Exception raised for invalid parameter values passed to an API.

    Attributes:
        element -- the element containing the invalid value
        invalid_value - the invalid value that was passed
        message -- user-readable explanation of the error
    """

    def __init__(self, *, element, invalid_value, message):
        self.element = element
        self.invalid_value = invalid_value
        self.message = message
        super().__init__(self.message)


def handle_api_response(func):
    """
    Define a decorator to wrap the processing of the Lambda handlers and return
    the appropriate API response, including:
    - the appropriate status code
    - the body of the response as a string-formatted JSON value
    """

    @wraps(func)
    def wrapped_func(*args, **kwargs):
        try:
            # return apiutils.create_success_object_response(func(*args, **kwargs))
            data = func(*args, **kwargs)
            if isinstance(data, BaseModel):
                return_data = data.dict(exclude_none=True)
            else:
                return_data = data
            return apiutils.create_success_object_response({"data": return_data})

        except BARDParameterValidationException as ex:
            bard_error = models.Error(
                code=ERROR_PREFERENCE_INVALID_PARAMETER_VALUE,
                title="Invalid parameter value",
                detail=f"The value '{ex.invalid_value}' is not valid for the parameter '{ex.element}'",
                source=models.Source(parameter=ex.element),
            )
            return apiutils.create_badrequest_object_response(bard_error)

        except Exception as ex:
            return apiutils.create_internalservererror_response(ex)

    return wrapped_func
