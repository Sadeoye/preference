from datetime import datetime
import json
from typing import Any, Dict

from http import HTTPStatus

import jsonpickle
from pydantic import BaseModel


class ResponseJSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        elif isinstance(o, BaseModel):
            return o.json(exclude_none=True)
        try:
            return super().default(o)
        except:
            return str(o)


def create_success_message_response(message: str) -> Dict[str, Any]:
    return generate_response(HTTPStatus.OK, {"message": message})


def create_success_object_response(body_json: Dict[str, Any]) -> Dict[str, Any]:
    return generate_response(HTTPStatus.OK, body_json)


def create_badrequest_message_response(message: str) -> Dict[str, Any]:
    return generate_response(HTTPStatus.BAD_REQUEST, {"message": message})


def create_badrequest_object_response(body_json: Dict[str, Any]) -> Dict[str, Any]:
    return generate_response(HTTPStatus.BAD_REQUEST, body_json)


def create_unauthorized_response() -> Dict[str, Any]:
    return generate_response(
        HTTPStatus.UNAUTHORIZED,
        {"message": "User is not authorized to perform this action"},
    )


def create_notfound_response(message: str) -> Dict[str, Any]:
    return generate_response(HTTPStatus.NOT_FOUND, {"message": message})


def create_internalservererror_response(message: str) -> Dict[str, Any]:
    return generate_response(HTTPStatus.INTERNAL_SERVER_ERROR, {"message": message})


def generate_response(http_status: int, body_json: Dict[str, Any]) -> Dict[str, Any]:
    if isinstance(body_json, BaseModel):
        encoded_json = body_json.json(exclude_none=True)
    else:
        encoded_json = json.dumps(body_json, cls=ResponseJSONEncoder)

    return {
        "statusCode": http_status,
        "headers": {
            "Access-Control-Allow-Headers": "Content-Type, Origin, X-Requested-With, Accept, Authorization, Access-Control-Allow-Methods, Access-Control-Allow-Headers, Access-Control-Allow-Origin",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "OPTIONS,POST,GET,PUT",
        },
        "body": encoded_json,
    }


def encode_to_json_object(input_object: Any) -> Any:
    jsonpickle.set_encoder_options("simplejson", use_decimal=True, sort_keys=False)
    jsonpickle.set_preferred_backend("simplejson")
    return jsonpickle.encode(input_object, unpicklable=False, use_decimal=True)
