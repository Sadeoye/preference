import json
import os

from aws_lambda_powertools import Logger
import pymysql

from preference_service.api import models

# TODO: Move these to SSM
DB_USERNAME = os.getenv("DB_USERNAME")
DB_PASSWORD = os.getenv("DB_PASSWORD")
DB_DATABASE = os.getenv("DB_DATABASE")

DB_HOST = os.getenv("DB_HOST")
CONNECTION_TIMEOUT = int(os.getenv("CONNECTION_TIMEOUT", "30"))
READ_TIMEOUT = float(os.getenv("READ_TIMEOUT", "30.0"))
WRITE_TIMEOUT = float(os.getenv("WRITE_TIMEOUT", "30.0"))

logger = Logger()


class PreferencesDB:
    def __init__(
        self,
        *,
        host: str = None,
        database: str = None,
        username: str = None,
        password: str = None,
    ):
        self.host = host if host else DB_HOST
        self.database = database if database else DB_DATABASE
        self.username = username if username else DB_USERNAME
        self.password = password if password else DB_PASSWORD
        self.connection = self.connect()

    def connect(self):
        """
        Connect to MySQL database instance, returning connection if successful
        """
        try:
            logger.info(f"Connecting to Database: {self.host}")
            logger.debug(
                {
                    "host": self.host,
                    "database": self.database,
                }
            )
            connection = pymysql.connect(
                host=self.host,
                user=self.username,
                password=self.password,
                db=self.database,
                cursorclass=pymysql.cursors.DictCursor,
                connect_timeout=CONNECTION_TIMEOUT,
                read_timeout=READ_TIMEOUT,
                write_timeout=WRITE_TIMEOUT,
            )

            return connection

        except Exception as ex:
            # TODO: Change this
            logger.error(f"ERROR: Error connecting to database: {ex}")

    def get_preference(
        self, *, user_id: int, client_code: str, category: str, item_name: str
    ):
        """
        Get a single preference value
        """
        try:
            if not self.connection.open:
                self.connection.connect()

            with self.connection.cursor() as cursor:
                sql = (
                    "SELECT"
                    "  `user_id`, `client_code`, `category`,"
                    "  `item_name`, `item_value`"
                    " FROM `preference`"
                    " WHERE user_id = %s"
                    " AND client_code = %s"
                    " AND category = %s"
                    " AND item_name = %s"
                )
                cnt = cursor.execute(sql, (user_id, client_code, category, item_name))
                logger.info(f"Number affected: {cnt}")
                row = cursor.fetchone()
                logger.info(f"Results: {row}")
                preference = models.Preference(
                    type="preference",
                    userId=int(row["user_id"]),
                    clientCode=row["client_code"],
                    category=row["category"],
                    itemName=row["item_name"],
                    itemValue=json.loads(row["item_value"]),
                )
                return preference

        except Exception as ex:
            # TODO: Change this
            logger.error(f"ERROR: Error selecting to database: {ex}")

    def close_connection(self):
        """
        Close database connection
        """
        try:
            self.connection.close()
        except Exception as ex:
            # TODO: Change this
            logger.error(f"ERROR: Error closing database connection: {ex}")
