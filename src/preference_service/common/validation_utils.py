"""Utility functions to provide validation"""


def is_int(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


def is_positive_int(s):
    try:
        return int(s) > 0
    except ValueError:
        return False
