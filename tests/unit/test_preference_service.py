from dataclasses import dataclass
from http import HTTPStatus
import json

import pytest

from preference_service.api import handlers

TEST_USER_ID = 123456
TEST_CLIENT_CODE = "BI"
TEST_CATEGORY = "Dev-iPhone"
TEST_ITEM_NAME = "TEXT_SETTINGS"


@pytest.fixture
def lambda_context():
    @dataclass
    class LambdaContext:
        function_name: str = "test"
        memory_limit_in_mb: int = 128
        invoked_function_arn: str = "arn:aws:lambda:eu-west-1:809313241:function:test"
        aws_request_id: str = "52fdfc07-2182-154f-163f-5f0f9a621d72"

    return LambdaContext()


@pytest.fixture()
def apigw_eventbase():
    """Generates API GW Template Event"""

    return {
        "body": "eyJ0ZXN0IjoiYm9keSJ9",
        "resource": "/{proxy+}",
        "path": "/preference",
        "httpMethod": "POST",
        "isBase64Encoded": "true",
        "queryStringParameters": {},
        "multiValueQueryStringParameters": {},
        "pathParameters": {},
        "stageVariables": {},
        "headers": {
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
            "Accept-Encoding": "gzip, deflate, sdch",
            "Accept-Language": "en-US,en;q=0.8",
            "Cache-Control": "max-age=0",
            "CloudFront-Forwarded-Proto": "https",
            "CloudFront-Is-Desktop-Viewer": "true",
            "CloudFront-Is-Mobile-Viewer": "false",
            "CloudFront-Is-SmartTV-Viewer": "false",
            "CloudFront-Is-Tablet-Viewer": "false",
            "CloudFront-Viewer-Country": "US",
            "Host": "1234567890.execute-api.us-east-1.amazonaws.com",
            "Upgrade-Insecure-Requests": "1",
            "User-Agent": "Custom User Agent String",
            "Via": "1.1 08f323deadbeefa7af34d5feb414ce27.cloudfront.net (CloudFront)",
            "X-Amz-Cf-Id": "cDehVQoZnx43VYQb9j2-nvCh-9z396Uhbp027Y2JvkCPNLmGJHqlaA==",
            "X-Forwarded-For": "127.0.0.1, 127.0.0.2",
            "X-Forwarded-Port": "443",
            "X-Forwarded-Proto": "https",
        },
        "multiValueHeaders": {
            "Accept": [
                "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"
            ],
            "Accept-Encoding": ["gzip, deflate, sdch"],
            "Accept-Language": ["en-US,en;q=0.8"],
            "Cache-Control": ["max-age=0"],
            "CloudFront-Forwarded-Proto": ["https"],
            "CloudFront-Is-Desktop-Viewer": ["true"],
            "CloudFront-Is-Mobile-Viewer": ["false"],
            "CloudFront-Is-SmartTV-Viewer": ["false"],
            "CloudFront-Is-Tablet-Viewer": ["false"],
            "CloudFront-Viewer-Country": ["US"],
            "Host": ["0123456789.execute-api.us-east-1.amazonaws.com"],
            "Upgrade-Insecure-Requests": ["1"],
            "User-Agent": ["Custom User Agent String"],
            "Via": ["1.1 08f323deadbeefa7af34d5feb414ce27.cloudfront.net (CloudFront)"],
            "X-Amz-Cf-Id": ["cDehVQoZnx43VYQb9j2-nvCh-9z396Uhbp027Y2JvkCPNLmGJHqlaA=="],
            "X-Forwarded-For": ["127.0.0.1, 127.0.0.2"],
            "X-Forwarded-Port": ["443"],
            "X-Forwarded-Proto": ["https"],
        },
        "requestContext": {
            "accountId": "123456789012",
            "resourceId": "123456",
            "stage": "prod",
            "requestId": "c6af9ac6-7b61-11e6-9a41-93e8deadbeef",
            "requestTime": "09/Apr/2015:12:34:56 +0000",
            "requestTimeEpoch": 1428582896000,
            "identity": {
                "apiKey": "",
                "userArn": "",
                "cognitoAuthenticationType": "",
                "caller": "",
                "userAgent": "Custom User Agent String",
                "user": "",
                "cognitoIdentityPoolId": "",
                "cognitoIdentityId": "",
                "cognitoAuthenticationProvider": "",
                "sourceIp": "127.0.0.1",
                "accountId": "",
            },
            "path": "/preference",
            "resourcePath": "/{proxy+}",
            "httpMethod": "POST",
            "apiId": "1234567890",
            "protocol": "HTTP/1.1",
        },
    }


# TODO: Change this
# def test_get_preference(apigw_eventbase, lambda_context):

#     apigw_eventbase["httpMethod"] = "GET"
#     apigw_eventbase[
#         "path"
#     ] = "/preference/user/{userId}/client/{clientCode}/category/{category}/item/{itemName}"
#     apigw_eventbase["pathParameters"] = {
#         "userId": TEST_USER_ID,
#         "clientCode": TEST_CLIENT_CODE,
#         "category": TEST_CATEGORY,
#         "itemName": TEST_ITEM_NAME,
#     }

#     ret = handlers.get_preference(apigw_eventbase, lambda_context)
#     data = json.loads(ret["body"])

#     assert ret["statusCode"] == HTTPStatus.OK
#     assert "preference" in ret["body"]
#     assert "preference" in data.keys()
#     preference = data["preference"]
#     assert "userId" in preference.keys()
#     assert preference["userId"] == TEST_USER_ID
#     assert "clientCode" in preference.keys()
#     assert preference["clientCode"] == TEST_CLIENT_CODE
#     assert "category" in preference.keys()
#     assert preference["category"] == TEST_CATEGORY


def test_get_preference_with_invalid_user_id_returns_error(apigw_eventbase, lambda_context):

    apigw_eventbase["httpMethod"] = "GET"
    apigw_eventbase[
        "path"
    ] = "/preference/user/{userId}/client/{clientCode}/category/{category}/item/{itemName}"
    apigw_eventbase["pathParameters"] = {
        "userId": "abc",
        "clientCode": TEST_CLIENT_CODE,
        "category": TEST_CATEGORY,
        "itemName": TEST_ITEM_NAME,
    }

    ret = handlers.get_preference(apigw_eventbase, lambda_context)
    assert ret["statusCode"] == HTTPStatus.BAD_REQUEST


def test_get_preference_is_not_get_returns_error(apigw_eventbase, lambda_context):
    apigw_eventbase["httpMethod"] = "POST"
    apigw_eventbase[
        "path"
    ] = "/preference/user/{userId}/client/{clientCode}/category/{category}/item/{itemName}"
    ret = handlers.get_preference(apigw_eventbase, lambda_context)
    assert ret["statusCode"] == HTTPStatus.INTERNAL_SERVER_ERROR
